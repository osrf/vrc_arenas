# REPOSITORY MOVED

## This repository has moved to

https://github.com/osrf/vrc_arenas

## Issues and pull requests are backed up at

https://osrf-migration.github.io/vrc_arenas-gh-pages/#!/osrf/vrc_arenas

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/vrc_arenas

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

